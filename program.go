package main

 import (
    "fmt"
    "image/jpeg"
    "os"
    "./raytraceLib"
 )
 
func main() {
    // Setup our scene
    raytraceLib.DefineViewFrustum(1, 1000)
    raytraceLib.CameraPosition = raytraceLib.Vector3{0, 0, -50}
    
    raytraceLib.ClearScene( raytraceLib.Vector3{1, 1, 1} )
    sphere := &raytraceLib.Sphere{ Center: raytraceLib.Vector3{0,-7,0}, Radius: 3 }
    raytraceLib.RenderObject( sphere )
    
    plane := &raytraceLib.Plane { raytraceLib.Vector3{0, -10, 0}, raytraceLib.Vector3{0, 1, 0}}
    raytraceLib.RenderObject(plane)
    
    tri := &raytraceLib.Triangle { raytraceLib.Vector3{10, 0, 10}, raytraceLib.Vector3{-10, 0, 10}, raytraceLib.Vector3{0, 10, 10}, raytraceLib.Vector3{0, 0, -1} }
    raytraceLib.RenderObject(tri)
    
    light := raytraceLib.Light { raytraceLib.Vector3{-10, 5, -10}, raytraceLib.Vector3{0.5, 0.5, 0.5} }
    light1 := raytraceLib.Light { raytraceLib.Vector3{0, 20, 0}, raytraceLib.Vector3{1, 0, 0} }
    raytraceLib.RenderLight(light)
    raytraceLib.RenderLight(light1)

    img := raytraceLib.Raytrace(800, 600, 3)
    
    // Save raytraced image to a file
    out, err := os.Create("./output.jpg")
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }
    
    var opt jpeg.Options
    opt.Quality = 100

    err = jpeg.Encode(out, img, &opt)
    if err != nil {
            fmt.Println(err)
            os.Exit(1)
    }

     fmt.Println("Generated image to output.jpg \n")
}

