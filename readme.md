#Simple Raytracing library

## Intoduction

This library is able to render images of virtual 3D scenes using a technique known as ray tracing.
The ray tracer has the ability to render shadows, simple diffuse lighting and multiple lights.
Supported primitives: Triangles, Spheres and Planes.


##Example usage:

    // Define min and max distances
    raytraceLib.DefineViewFrustum(1, 1000.0)
    // Set camera position
    raytraceLib.CameraPosition = raytraceLib.Vector3{0, 0, 10}
    
    // Call clear to set background color and clean the scene
    raytraceLib.ClearScene( raytraceLib.Vector3{1, 1, 1} )
    
    // Create objects such as spheres, planes, triangles....
    sphere := &raytraceLib.Sphere{ Center: raytraceLib.Vector3{0,-7,0}, Radius: 3 }
    raytraceLib.RenderObject( sphere )
    
    plane := &raytraceLib.Plane { raytraceLib.Vector3{0, -10, 0}, raytraceLib.Vector3{0, 1, 0}}
    raytraceLib.RenderObject(plane)
    
    // Render the scene
    raytraceLib.Raytrace(800, 600, 3)