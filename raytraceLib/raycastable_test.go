package raytraceLib_test

import "math"
import "testing"
import "../raytraceLib"

func TestSphere(t *testing.T) {
    v := &raytraceLib.Sphere{ raytraceLib.Vector3{0, 0, 0}, 1 }
    intersect, d := v.Raycast( raytraceLib.Vector3{0, 0, -5}, raytraceLib.Vector3{0, 0, 1} )
    if !intersect || math.Abs(d - 4) > 0.001 {
        t.Error("Sphere collision failed -  we expected it to succeed. ", intersect, d)
    }
}

func TestTriangle(t *testing.T) {
    v := &raytraceLib.Triangle{ raytraceLib.Vector3{5, 0, 0}, raytraceLib.Vector3{-5, 0, 0}, raytraceLib.Vector3{0, 5, 0}, raytraceLib.Vector3{0, 0, -1} }
    intersect, d := v.Raycast( raytraceLib.Vector3{0, 0, -5}, raytraceLib.Vector3{0, 0, 1} )
    if !intersect || math.Abs(d - 5) > 0.001 {
        t.Error("Sphere collision failed -  we expected it to succeed.", intersect, d)
    }
}

func TestPlane(t *testing.T) {
    v := &raytraceLib.Plane{ raytraceLib.Vector3{0, 0, 0}, raytraceLib.Vector3{0, 0, -1}}
    intersect, _ := v.Raycast( raytraceLib.Vector3{0, 0, -10}, raytraceLib.Vector3{0, 1, 0} )
    if intersect{
        t.Error("Plane collision succeeded -  we expected it to fail.")
    }
}

