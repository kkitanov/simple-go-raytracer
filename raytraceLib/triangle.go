package raytraceLib

type Triangle struct {
	P1, P2, P3 Vector3
	Normal Vector3
}

func (triangle *Triangle) Raycast( origin, dir Vector3 ) (intersect bool, t float64) {
	// Check if the ray intersects the plane that the triangle is inside
	
	plane := Plane { triangle.P1, triangle.Normal }
	
	intersect, t =  plane.Raycast(origin, dir)
	
	if intersect {
		// Check if the point is inside the triangle
		P := Add( origin, Mul(t, dir))
		
		edge0 := Sub(triangle.P2, triangle.P1)
		vp0 := Sub(P, triangle.P1) 
		C := Cross(edge0, vp0)
		if ( Dot(triangle.Normal , C) < 0) {
			 return false, t
		}
		
		edge1 := Sub(triangle.P3, triangle.P2)
		vp1 := Sub(P, triangle.P2) 
		C = Cross(edge1, vp1)
		if ( Dot(triangle.Normal , C) < 0) {
			 return false, t
		}
		
		edge2 := Sub(triangle.P1, triangle.P3)
		vp2 := Sub(P, triangle.P3) 
		C = Cross(edge2, vp2)
		if ( Dot(triangle.Normal , C) < 0) {
			 return false, t
		}
	}
		
		
	return intersect, t
}

func (triangle *Triangle) GetNormal( point Vector3 ) Vector3 {
	return triangle.Normal
}