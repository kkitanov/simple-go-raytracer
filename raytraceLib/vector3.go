package raytraceLib

import "math"

type Vector3 struct {
	X, Y, Z float64
}

func Sub( first, other Vector3 ) Vector3 {
	return Vector3{first.X - other.X, first.Y - other.Y, first.Z - other.Z}
}

func Add( first, other Vector3 ) Vector3 {
	return Vector3{first.X + other.X, first.Y + other.Y, first.Z + other.Z}
}

func Dot( first, other Vector3 ) float64 {
	return first.X * other.X + first.Y * other.Y + first.Z * other.Z
}

func Cross( first, other Vector3 ) (result Vector3)  {
	result.X = first.Y*other.Z-first.Z*other.Y;
	result.Y = first.Z*other.X-first.X*other.Z;
	result.Z = first.X*other.Y-first.Y*other.X;

	return result;
}

func Mul(scalar float64, vector Vector3) Vector3 {
	return Vector3{scalar*vector.X, scalar*vector.Y, scalar*vector.Z}
}

func Div( scalar float64, vector Vector3) Vector3 {
	return Mul( 1.0 / scalar, vector)
}

func Length(vector Vector3) float64 {
	return math.Sqrt(Dot(vector, vector))
}

func Normalize(vector Vector3) Vector3 {
	return Div(Length(vector), vector);
}

func Clamp(vector Vector3, min, max float64) Vector3 {
	if vector.X < min {
		vector.X = min
	}
	if vector.X > max {
		vector.X = max;
	}
	
	if vector.Y < min {
		vector.Y = min
	}
	if vector.Y > max {
		vector.Y = max;
	}
	
	if vector.Z < min {
		vector.Z = min
	}
	if vector.Z > max {
		vector.Z = max;
	}
	
	return vector
}
	