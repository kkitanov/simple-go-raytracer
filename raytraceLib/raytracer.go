package raytraceLib

import "image"
import "image/color"
import "runtime"
import "sync"

var width, height int
var nearPlaneDist, farPlaneDist float64

var CameraPosition  = Vector3{0, 0, -10}
var CameraDirection = Vector3{0, 0, 1}
var CameraRight     = Vector3{1, 0, 0}
var CameraUp        = Vector3{0, 1, 0}

// This slice will store our scene
var raycastbles []Raycastable = make([]Raycastable, 0)
var lights      []Light       = make([]Light, 0)
var backgroundColor Vector3


// Defines the view frustum, based on screen resoltuion and distances
func DefineViewFrustum( near, far float64) {
	nearPlaneDist = near
	farPlaneDist = far
}

// Deletes all objects from the scene and sets the background Color - call every frame
func ClearScene( backgroundColor Vector3) {
	backgroundColor = backgroundColor
	raycastbles = make([]Raycastable, 0)
	lights = make([]Light, 0)
}

func RenderObject(obj Raycastable) {
	raycastbles = append( raycastbles, obj)
}

func RenderLight(obj Light){
	lights = append( lights, obj)
}

// Performs raytracing over the scene with the selected quality settings.
// Returns an RGBA object that contains the produced image
func Raytrace( width, height, antiAliasingSamples int) *image.RGBA {
	// Set the maximum number of processors used by the goroutines    
    runtime.GOMAXPROCS(runtime.NumCPU())	
	
	imgRect := image.Rect(0, 0, width, height)
    img := image.NewRGBA(imgRect)
      
    var wg sync.WaitGroup              
    for i:= 0; i < width; i++ {
        for j:=0; j < height; j++{
			// Each pixel is traced in separate goroutine
            wg.Add(1)
            go func (i, j int){
                defer wg.Done()
                col := RaytracePixel(i, j, width, height, antiAliasingSamples)
                img.Set(i, j, color.RGBA{uint8(col.X*255), uint8(col.Y*255), uint8(col.Z*255), 1}) 
            } (i, j)
        }
    }

	// Wait for all render to complete
    wg.Wait()
	
	return img
}

// Returns the pixel color for pixel x,y
func RaytracePixel(x, y, width, height, antiAliasingSamples int) (color Vector3) {
	// We need to get the coordinates of the near plane from the pixel positions
	// View angle is determined by distance to the near plane and screen resoltuion
	
	dx := float64(1.0) / float64(width)
	dy := float64(1.0) / float64(height)
	aspect := float64(width) / float64(height)

	for i := 0; i < antiAliasingSamples; i++ {
		for j := 0; j < antiAliasingSamples; j++ {
			viewPlaneX := float64(x) / float64(width) + dx*float64(i)/float64(antiAliasingSamples)
			viewPlaneY := float64(y) / float64(height) + dy*float64(j)/float64(antiAliasingSamples)
			
			// The view rect has a size of 1 unit, centered at 0,0
			viewPlaneX = (viewPlaneX-0.5)*aspect
			// Invert the Y axis as in image space positive is down and negative is up. In all other spaces its the reverse.
			viewPlaneY = -(viewPlaneY-0.5)
			
			origin := CameraPosition
			dir    := Add( Mul(viewPlaneX, CameraRight),  Mul(viewPlaneY, CameraUp))
			dir     = Add( dir, Mul(nearPlaneDist, CameraDirection) )
			dir     = Normalize(dir)
			
			// Primary ray
			closestDist, closest := ShootRay(origin, dir, farPlaneDist, false)
			
			// Shoot rays to determine light color and shadows				
			if closest != nil {
				intersecPos := Add( origin, Mul( closestDist, dir) )
				normal := closest.GetNormal(intersecPos)
				
				for _, light := range(lights){
					// Check if point is in shadow for this light
					objToLight := Sub(light.Position, intersecPos)
					objToLightLen := Length(objToLight)
					objToLightNorm := Div(objToLightLen, objToLight)
					
					offsetPosTowardsLight := Add( intersecPos, Mul(0.001, objToLightNorm) )
					_, occluder := ShootRay( offsetPosTowardsLight, objToLightNorm, objToLightLen, true)
					
					if ( occluder == nil ){
						diffuseTerm := Dot( objToLightNorm, normal)
						if ( diffuseTerm > 0 ){
							diffuse := Mul (diffuseTerm, light.Color )
							color = Add(color, diffuse)		
						}		
					}
				}
			}
		}
	}
	
	return Clamp( Div( float64(antiAliasingSamples*antiAliasingSamples), color ), 0, 1)
}

// Shoots a ray into the scene and returns the closest object that collides with (if any).
// Alternatively. if its a shadowRay it will return the first object that collides with the light
func ShootRay(origin, dir Vector3, maxDist float64, shadowRay bool) (closestDist float64, closest Raycastable)  {
	closestDist = maxDist
	
	for _, obj := range(raycastbles) {
		intersect, t := obj.Raycast(origin, dir)
		if ( intersect && t < closestDist ){
			closestDist = t;
			closest = obj;
			
			if ( shadowRay ){
				return closestDist, closest
			}
		}
	}	
	
	return closestDist, closest	
}
