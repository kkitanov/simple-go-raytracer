package raytraceLib

type Plane struct {
	Position, Normal Vector3
}


func (plane *Plane) Raycast( origin, dir Vector3 ) (intersect bool, t float64) {
	// Plane equation  (Position - p). normal = 0
	// Substitue p for the ray equation : origin + t*dir = p
	// Solve for t
	
	dotProduct := Dot( dir, plane.Normal )
	if ( dotProduct == 0) {
		intersect = false
	} else{
		t = Dot(plane.Normal, Sub(plane.Position, origin)) / dotProduct 
	
		intersect = (t >= 0)
	}

	return intersect, t
}

func (plane *Plane) GetNormal( point Vector3 ) Vector3 {
	return plane.Normal
}