package raytraceLib

import "math"

type Sphere struct {
	Center Vector3
	Radius float64
}


func (sphere *Sphere) Raycast( origin, dir Vector3 ) (intersect bool, t float64) {
	// Sphere equation |(point - center)| = radius
	// Substitue point for the ray equation : origin + t*dir = point
	// Solve for t
	
	var radiSquare   = sphere.Radius * sphere.Radius
	var fromCenterToOrigin = Sub(origin, sphere.Center)
	var dotProd = Dot(fromCenterToOrigin, dir)
	
	var discriminant = dotProd*dotProd - Dot(fromCenterToOrigin, fromCenterToOrigin) + radiSquare
	
	if discriminant < 0 {
		intersect = false
	} else{
		t = -dotProd - math.Sqrt(discriminant)
			 	   
		if ( t > 0 ){
			intersect = true
		}
	}

	return intersect, t
}

func (sphere *Sphere) GetNormal( point Vector3 ) Vector3 {
	return Normalize( Sub(point, sphere.Center) )
}