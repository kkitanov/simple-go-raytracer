package raytraceLib_test

import "math"
import "testing"
import "../raytraceLib"

func TestVector3Length(t *testing.T) {
    v := raytraceLib.Vector3{1, 2, 2}
    l := raytraceLib.Length(v)
    if math.Abs(l - 3) > 0.001 {
        t.Error("Length({1,2,2}) returned", l, ", we expected 3")
    }
}

func TestVector3AddSub(t *testing.T) {
    v := raytraceLib.Add(raytraceLib.Vector3{1, 0, 0}, raytraceLib.Vector3{0, 0, 1})
    v = raytraceLib.Sub(v, raytraceLib.Vector3{0, -1, 0})
    
    if math.Abs(v.X - 1) > 0.001 {
        t.Error("Vector addition and substraction returned", v.X, ", we expected 1")
    }
    
    if math.Abs(v.Y - 1) > 0.001 {
        t.Error("Vector addition and substraction returned", v.Y, ", we expected 1")
    }
    
    if math.Abs(v.Y - 1) > 0.001 {
        t.Error("Vector addition and substraction returned", v.Z, ", we expected 1")
    }
}

func TestVector3Normalize(t *testing.T) {
    v := raytraceLib.Normalize(raytraceLib.Vector3{10, 0, 0})
    l := raytraceLib.Length(v)
    if math.Abs(l - 1) > 0.001 {
        t.Error("Normalize({10,0,0}) returned", l, ", we expected 1")
    }
}
