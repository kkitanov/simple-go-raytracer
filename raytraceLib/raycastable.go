package raytraceLib

type Raycastable interface {
	// Returns whether the ray will intersect the Raycastable object and the point at which the collision happens
	Raycast( origin, dir Vector3 ) (intersect bool, t float64)
	// Returns the normal vector at the point (world coordinates)
	GetNormal(point Vector3) Vector3
}

