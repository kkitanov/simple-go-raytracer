package raytraceLib

// Simple omni directional light
type Light struct {
	Position Vector3
	// Diffuse color
	Color Vector3
}

